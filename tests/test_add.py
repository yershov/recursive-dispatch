#!/usr/bin/env python

import unittest
import bug

class Adder(bug.Adder):
    def __call__(self, first, second, visitor):
        visitor(Data(first.value + second.value))

class Data(bug.Data):
    def __init__(self, value):
        super().__init__()
        self.__value = value

    @property
    def value(self):
        return self.__value

class Store:
    def __init__(self):
        self.result = None

    def __call__(self, data):
        self.result = data.value

class TestTensor(unittest.TestCase):
    def test_add_int(self):
        store = Store()
        bug.add(Data(1), Data(2), Adder(), store)
        self.assertEqual(store.result, 3)

    def test_add3_sep_int(self):
        store = Store()
        bug.add3_sep(Data(1), Data(2), Data(3), Adder(), Adder(), store)
        self.assertEqual(store.result, 6)

    def test_add3_int(self):
        store = Store()
        bug.add3(Data(1), Data(2), Data(3), Adder(), store)
        self.assertEqual(store.result, 6)

    def test_add3_sep_same_int(self):
        store = Store()
        adder = Adder()
        bug.add3_sep(Data(1), Data(2), Data(3), adder, adder, store)
        self.assertEqual(store.result, 6)

    def test_add_string(self):
        store = Store()
        bug.add(Data("1"), Data("2"), Adder(), store)
        self.assertEqual(store.result, "12")

    def test_add3_sep_string(self):
        store = Store()
        bug.add3_sep(Data("1"), Data("2"), Data("3"), Adder(), Adder(), store)
        self.assertEqual(store.result, "123")

    def test_add3_string(self):
        store = Store()
        bug.add3(Data("1"), Data("2"), Data("3"), Adder(), store)
        self.assertEqual(store.result, "123")

    def test_add3_sep_same_string(self):
        store = Store()
        adder = Adder()
        bug.add3_sep(Data("1"), Data("2"), Data("3"), adder, adder, store)
        self.assertEqual(store.result, "123")

if __name__ == '__main__':
    unittest.main()
