#!/usr/bin/env python

import setuptools
from pybind11.setup_helpers import Pybind11Extension# , build_ext

pybind_modules = [
    Pybind11Extension("bug", [
        "pybind/Algorithm.cpp",
        "pybind/PyBind.cpp",
    ])
]

with open("README.md", "r") as fh:
    long_description = fh.read()

package_dir={'': 'src'}
packages = setuptools.find_packages(where='src')

setuptools.setup(name='recursive-dispatch',
                 version='0.1',
                 author='Dmitry Yershov',
                 author_email='dmitry.s.yershov@gmail.com',
                 description='Recursive Dispatch Bug Report',
                 long_description=long_description,
                 long_description_content_type="text/markdown",
                 url="https://bitbucket.org/yershov/recursive-dispatch/src/main/",
                 package_dir=package_dir,
                 packages=packages,
                 install_requires=['pybind11'],
                 # cmdclass={"build_ext": build_ext},
                 ext_modules=pybind_modules,
                 )
