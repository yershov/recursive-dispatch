#include "Algorithm.hpp"
#include "Data.hpp"

#include <pybind11/functional.h>
#include <pybind11/pybind11.h>

#include <exception>

namespace {

namespace py = pybind11;

// Adder trampoline
class PyAdder : public bug::Adder
{
public:
    PyAdder() = default;

    void operator()(const bug::Data& first, const bug::Data& second, const bug::DataVisitor& visitor) const override {
        PYBIND11_OVERRIDE_PURE_NAME(void, bug::Adder, "__call__", operator(), first, second, visitor);
    }
};

// Data trampoline
class PyData : public bug::Data
{
public:
    PyData() = default;
};

} // end of anonymous namespace

PYBIND11_MODULE(bug, m) {
    pybind11::class_<bug::Adder, PyAdder>(m, "Adder")
        .def(pybind11::init<>())
        .def("__call__", &bug::Adder::operator());

    m.def("add", &bug::add);
    m.def("add3", &bug::add3);
    m.def("add3_sep", &bug::add3_sep);

    pybind11::class_<bug::Data, PyData>(m, "Data")
        .def(pybind11::init<>());
}
