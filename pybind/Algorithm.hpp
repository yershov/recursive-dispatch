#pragma once

#include "Data.hpp"

namespace bug {

// Abstract Adder
class Adder
{
public:
    virtual ~Adder() = default;

    // interface to add two data objects, the result is passes to data visitor
    virtual void operator()(const Data& first, const Data& second, const DataVisitor& visitor) const = 0;

protected:
    Adder() = default;
private:
    Adder(const Adder&) = delete;
};

// An algorithm that invokes adder once. It works as intended.
void add(const Data& first, const Data& second, const Adder& adder, const DataVisitor& visitor);

// An algorithm that invokes adder recursively. The second dispatch of the operator() cannot find overloaded __call__.
void add3(const Data& first, const Data& second, const Data& third, const Adder& adder, const DataVisitor& visitor);

// An algorithm that invokes adder recursively using separate adders. If the same adder object is used for both adder arguments, then the second dispatch fails.
void add3_sep(const Data& first, const Data& second, const Data& third, const Adder& adder1, const Adder& adder2, const DataVisitor& visitor);

} // namespace bug
