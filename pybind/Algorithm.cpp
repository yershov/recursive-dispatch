#include "Algorithm.hpp"

namespace bug {

void add(const Data& first, const Data& second,  const Adder& adder, const DataVisitor& visitor)
{
    adder(first, second, visitor);
}

void add3(const Data& first, const Data& second, const Data& third,  const Adder& adder, const DataVisitor& visitor)
{
    adder(first, second, [&] (const Data& first_plus_second) {
        adder(first_plus_second, third, visitor);
    });
}

void add3_sep(const Data& first, const Data& second, const Data& third, const Adder& adder1, const Adder& adder2, const DataVisitor& visitor)
{
    adder1(first, second, [&] (const Data& first_plus_second) {
        adder2(first_plus_second, third, visitor);
    });
}

} // namespace bug
