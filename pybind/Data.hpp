#pragma once

#include <functional>

namespace bug {

// Abstract data
class Data
{
public:
    virtual ~Data() = default;
protected:
    Data() = default;
private:
    Data(const Data&) = delete;
};

// DataVisitor that can be used inside algorithms
using DataVisitor = std::function<void (const Data&)>;

} // namespace bug
